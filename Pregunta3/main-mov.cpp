#include<iostream>

#include <fstream>
#include <map>
#include <deque>
#include <string>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace std;

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}


int readMarks(std::string filename, std::map<int, int>& msecs) {
    std::string num, time, line;
    int val, coma;
    int maxFrame = 0;
    std::ifstream file(filename, std::ios::in);
    if(file.is_open()) {
        while (std::getline(file, line)) {
            trim(line);
            if(line == "")
                continue;
            coma = line.find(",");
            num = line.substr(0, coma);
            trim(num);
            time = line.substr(coma+1);
            trim(time);
            val = std::stoi(num);
            msecs[val] = std::stoi(time);
            if(val > maxFrame)
                maxFrame = val;
        }
        file.close();
    }
    return maxFrame;
}



double IoU(cv::Rect &r1, cv::Rect &r2) {
    int maxx = r1.x + r1.width, maxy = r1.y + r1.height;
    cv::Rect inter = r1 & r2;
    cv::Mat im = cv::Mat::zeros(maxy, maxx, CV_8UC1);
    cv::rectangle(im, r1, cv::Scalar(255), -1);
    cv::rectangle(im, r2, cv::Scalar(255), -1);
    float inter_area = inter.area(), union_area = cv::countNonZero(im);
    return inter_area/union_area;
}

void readGTBoundingBoxes(std::map<int, cv::Rect> &gt_bboxes) {
    std::string filename = "annotate.csv";
    std::string sframe, sx, sy, sw, sh, line;
    cv::Rect r;
    int frame, coma;
    std::ifstream file(filename, std::ios::in);
    if(file.is_open()) {
        while (std::getline(file, line)) {
            trim(line);
            if(line == "")
                continue;
            coma = line.find(",");
            sframe = line.substr(0, coma);
            trim(sframe);
            frame = std::stoi(sframe);
            line = line.substr(coma+1);
            coma = line.find(",");
            sx = line.substr(0, coma);
            trim(sx);
            r.x = std::stoi(sx);
            line = line.substr(coma+1);
            coma = line.find(",");
            sy = line.substr(0, coma);
            trim(sy);
            r.y = std::stoi(sy);
            line = line.substr(coma+1);
            coma = line.find(",");
            sw = line.substr(0, coma);
            trim(sw);
            r.width = std::stoi(sw);
            sh = line.substr(coma+1);
            trim(sh);
            r.height = std::stoi(sh);

            gt_bboxes[frame] = r;
        }
        file.close();
    }

}

void evaluateResult(std::map<int, cv::Rect> &algorithm_bboxes,
                    std::map<int, cv::Rect> &gt_bboxes) {

    if(algorithm_bboxes.size() != gt_bboxes.size()) {
        std::cout << "Error: The number of bounding boxes for algorithm and groundtruth shall be the same, as they represent the number of frames of the sequence!" << std::endl;
        return;
    }

    std::map<int, cv::Rect>::iterator iter = gt_bboxes.begin();
    std::vector<float> IoUs;

    float vIoU, mean_IoU = 0.0, sd_IoU = 0, min = 2.0, max = -1.0;
    for(; iter != gt_bboxes.end(); iter++) {
        if(algorithm_bboxes.count(iter->first) == 0)
            std::cout << "Error: Entry not found for frame " << iter->first
                      << "in algorithm bounding boxes map!"  << std::endl;

        vIoU = IoU(algorithm_bboxes[iter->first], iter->second);
        if(vIoU < min)
            min = vIoU;
        if(vIoU > max)
            max = vIoU;
        mean_IoU += vIoU;
        IoUs.push_back(vIoU);
        //std::cout << "\tFor frame " << iter->first << " in groundtruth, IoU is: " << vIoU << std::endl;
    }
    mean_IoU /= gt_bboxes.size();
    std::cout << "\tMin IoU: " << min << std::endl;
    std::cout << "\tMax IoU: " << max << std::endl;
    std::cout << "\tMean IoU: " << mean_IoU << std::endl;

    for(uint i=0; i<IoUs.size(); ++i)
        sd_IoU += (IoUs[i] - mean_IoU)*(IoUs[i] - mean_IoU);
    sd_IoU /= gt_bboxes.size() - 1;
    sd_IoU = sqrt(sd_IoU);
    std::cout << "\tStandard Deviation IoU: " << sd_IoU << std::endl;


}


int main(int argc, char *argv[]) {

    std::map<int, cv::Rect> gt_bboxes;
    std::map<int, cv::Rect> alg_bboxes; //Guarde las cajas englobantes por frame aca.

    readGTBoundingBoxes(gt_bboxes); // Lee la groundtruth y la almacena en gt_bboxes.

 //WRITE YOUR CODE HERE!!

    evaluateResult(alg_bboxes, gt_bboxes);

    return 0;
}

