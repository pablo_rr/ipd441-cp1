import cv2
import numpy as np
import sys

from os import listdir
from os.path import isfile, join

def get_blobs(labels):
	bboxes = {}
	i, j = 0, 0
	
	while j < labels.shape[0]:
		i = 0
		while i < labels.shape[1]:
			label = labels[j][i]
			if label > 0:
				if label not in bboxes.keys():
					r = [i, j, 1, 1]
					bboxes[label] = r
				else:
					r = bboxes[label]
					x = r[0] + r[2] - 1
					y = r[1] + r[3] - 1
					if i < r[0]:
						r[0] = i 
					if i > x:
						x = i 
					if j < y:
						r[1] = j
					if j > y:
						y = j 
					r[2] = x - r[0] + 1
					r[3] = y - r[1] + 1
			i += 1
		j += 1
	return bboxes

def paint_rectangles(img, bboxes):
	for bbox in bboxes.values():
		start_point = (bbox[0], bbox[1])
		end_point = (bbox[0] + bbox[2], bbox[1] + bbox[3])
		img = cv2.rectangle(img, start_point, end_point, (0, 0, 255), 2)
			
	return img

def IoU(r0, r1):
	maxx = r0[0] + r0[2]
	maxy = r0[1] + r0[3]

	im = np.zeros((maxy, maxx), np.uint8)
	
	start_point = (r0[0], r0[1])
	end_point = (r0[0] + r0[2], r0[1] + r0[3])    
	im = cv2.rectangle(im, start_point, end_point, (255), -1)

	start_point = (r1[0], r1[1])
	end_point = (r1[0] + r1[2], r1[1] + r1[3])
	im = cv2.rectangle(im, start_point, end_point, (255), -1)
	
	x = np.max((r0[0], r1[0]))
	y = np.max((r0[1], r1[1]))
	w = np.min((r0[0] + r0[2], r1[0] + r1[2])) - x
	h = np.min((r0[1] + r0[3], r1[1] + r1[3])) - y
	
	inter_area = np.abs(w * h)
	union_area = cv2.countNonZero(im)
	return inter_area / union_area

def read_marks(filename):
	msecs = {}
	with open(filename) as f:
		lines = f.readlines()
		for l in lines:
			items = l.split(',')
			msecs[int(items[0])] = int(items[1])
	return msecs

def read_GT_BoundingBoxes(filename='annotate.csv'):
	gt_bboxes = {}
	with open(filename) as f:
		lines = f.readlines()
		for l in lines:
			items = l.split(',')
			gt_bboxes[int(items[0])] = tuple([int(i) for i in items[1:]])
	return gt_bboxes

def evaluate_result(alg_bboxes, gt_bboxes):
	if len(alg_bboxes) != len(gt_bboxes):
		print('Error: The number of bounding boxes for algorithm and groundtruth shall be the same, as they represent the number of frames of the sequence!')
		return

	IoUs = []
	mean_IoU, sd_IoU = (0.0, 0)
	vmin, vmax = (2.0, -1.0)
	for key, bbox in gt_bboxes.items():
		vIoU = IoU(alg_bboxes[key], bbox)
		if vIoU < vmin:
			vmin = vIoU
		if vIoU > vmax:
			vmax = vIoU
		mean_IoU += vIoU
		IoUs.append(vIoU)
	mean_IoU /= len(gt_bboxes)
	print('\tMin IoU: {}'.format(vmin))
	print('\tMax IoU: {}'.format(vmax))
	print('\tMean IoU: {}'.format(mean_IoU))

	for iou in IoUs:
		sd_IoU += (iou - mean_IoU) * (iou - mean_IoU)
	sd_IoU /= (len(gt_bboxes) - 1)
	sd_IoU = np.sqrt(sd_IoU)
	print('\tStandard Deviation IoU: {}'.format(sd_IoU))

# Función para obtener imagen de fondo
def gen_background(seq_folder='SAMPLES/SEQUENCE', marks_file='marks.csv', trange=(2001, 16000), save=False):
	# Se enlista la ruta a cada imagen en el directorio de secuencia
	seq_path = sorted([join(seq_folder, f) for f in listdir(seq_folder) if isfile(join(seq_folder, f))])
	# Se selecciona los frames donde el sujeto posee mayor movimiento
	marks = read_marks(marks_file)
	msec = np.array(list(marks.values()))
	filt = (msec >= trange[0])*(msec < trange[1])
	fframes = np.array(list(marks.keys()))[filt].tolist() # Frames filtrados
	
	# Se obtiene la imagen de fondo haciendo uso de la función accumulate weighted
	avg = np.float32(cv2.imread(seq_path[fframes[0]]))
	for f in fframes[1:]:
		seq = cv2.imread(seq_path[f])

		cv2.accumulateWeighted(seq, avg, 0.009)
		res = cv2.convertScaleAbs(avg)

	# Se guarda la imagen de fondo para ser cargada directamente
	if save:
		cv2.imwrite('bg.png', res)

	# Se retorna imagen de fondo
	return res
	
# Función para obtener las cajas englobantes del sujeto en la escena
def gen_alg_bboxes(seq_folder='SAMPLES/SEQUENCE', bg=None, bg_file='bg.png', savefile='alg_bboxes.csv'):
	# Se enlista la ruta a cada imagen en el directorio de secuencia
	seq_path = sorted([join(seq_folder, f) for f in listdir(seq_folder) if isfile(join(seq_folder, f))])
	nframes = len(seq_path)

	# Si no hay imagen de background como argumento, cargar de archivo como escala de grises
	if bg == None:
		bg = cv2.cvtColor(cv2.imread(bg_file), cv2.COLOR_BGR2GRAY)
	else:
		bg = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)

	# Se crea/limpia archivo para almacenar cajas englobantes
	bboxes_file = open(savefile, 'w')
	bboxes_file.close()
	# Se inicializa diccionario que tiene las cajas englobantes del algoritmo
	alg_bboxes = {}

	# Se itera sobre la secuencia
	for key, fseq in zip(range(1, nframes + 1), seq_path):
		# Se carga imagen de secuencia seq y se convierte a escala de grises
		seq = cv2.imread(fseq)
		seq = cv2.cvtColor(seq, cv2.COLOR_BGR2GRAY)
		
		# Se aplica filtro gaussiano a fondo y secuencia
		bbg = cv2.GaussianBlur(bg, (3, 3), 0) #blurred bg
		seq = cv2.GaussianBlur(seq, (3, 3), 0)

		# Se resta el fondo de la imagen de secuencia y se segmenta usando OTSU. Es necesario cambiar el tipo de dato
		fg_mask = np.uint8(np.abs(np.int16(seq) - np.int16(bbg)))
		_, fg_seg = cv2.threshold(fg_mask, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

		# Se aplican operaciones morfológicas para corregir defectos en la segmentación
		kernel = np.ones((3, 3), np.uint8)
		fg_open = cv2.morphologyEx(fg_seg, cv2.MORPH_OPEN, kernel, iterations=1)

		kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7))
		fg_close = cv2.morphologyEx(fg_open, cv2.MORPH_CLOSE, kernel, iterations=1)

		# Se determinan los contornos en la imagen
		(contornos, _) = cv2.findContours(fg_close.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		dest = np.zeros(fg_close.shape, np.uint8)
		
		try:
			# Se selecciona el contorno de mayor área y se dibuja en la imagen destino
			cont = max(contornos, key=lambda x: cv2.contourArea(x))
			dest = cv2.drawContours(dest, [cont], -1, (255), thickness=cv2.FILLED)

			# Se obtiene el componente conectado (todos los otros componentes han sido eliminados de la escena)
			_, labels, _, _ = cv2.connectedComponentsWithStats(dest)
			bbox = get_blobs(labels)
			rect = bbox[list(bbox.keys())[0]]

			# Se actualiza el diccionario y el archivo destino
			alg_bboxes[key] = rect
			with open(savefile, 'a') as f:
				f.write('{:d},{:d},{:d},{:d},{:d}\n'.format(key, rect[0], rect[1], rect[2], rect[3]))

		except Exception as e:
			print(e)
			pass

	return alg_bboxes

# Función de visualización de cajas englobantes en la escena. Amarillo: gt - Verde: algoritmo
def vis_results(alg_bboxes, gt_bboxes, seq_folder='SAMPLES/SEQUENCE'):
	# Se enlista la ruta a cada imagen en el directorio de secuencia
	seq_path = sorted([join(seq_folder, f) for f in listdir(seq_folder) if isfile(join(seq_folder, f))])
	for key, fseq in zip(gt_bboxes.keys(), seq_path):
		# Se lee imagen de secuencia
		seq = cv2.imread(fseq)

		# Se dibujan cajas englobantes para ground truth y el algoritmo
		x0, y0 = (gt_bboxes[key][0], gt_bboxes[key][1])
		x1, y1 = (gt_bboxes[key][0] + gt_bboxes[key][2], gt_bboxes[key][1] + gt_bboxes[key][3])
		cv2.rectangle(seq, (x0, y0), (x1, y1), (0, 255, 255), thickness=2)

		x0, y0 = (alg_bboxes[key][0], alg_bboxes[key][1])
		x1, y1 = (alg_bboxes[key][0] + alg_bboxes[key][2], alg_bboxes[key][1] + alg_bboxes[key][3])
		cv2.rectangle(seq, (x0, y0), (x1, y1), (0, 255, 0), thickness=2)

		# Se muestra la imagen actual
		cv2.imshow('Seq', seq)

		# Salir de visualización presionando 'q' o Esc
		k = cv2.waitKey(20) & 0xFF
		if k == ord('q') or k == 27:
			break

	cv2.destroyAllWindows()

# Se leen las cajas englobantes ground-truth y la del algoritmo propio
gt_bboxes = read_GT_BoundingBoxes(filename='annotate.csv')
alg_bboxes = read_GT_BoundingBoxes(filename='alg_bboxes.csv') # not ground-truth

# Para cualquiera de estas dos opciones es necesaria la sequencia en SAMPLES/SEQUENCE
if len(sys.argv) > 1:
	# Corre todo el algoritmo desde el principio
	if '-all' in sys.argv:
		bg = gen_background()
		alg_bboxes = gen_alg_bboxes(bg=bg)
		vis_results(alg_bboxes, gt_bboxes)

	# Visualización de resultados
	if '-vis' in sys.argv:
		vis_results(alg_bboxes, gt_bboxes)

# Evaluación del resultado
evaluate_result(alg_bboxes, gt_bboxes)