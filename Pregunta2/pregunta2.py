import cv2
import matplotlib.pyplot as plt
import numpy as np

def get_blobs(labels):
	bboxes = {}
	i, j = 0, 0
	
	while j < labels.shape[0]:
		i = 0
		while i < labels.shape[1]:
			label = labels[j][i]
			if label > 0:
				if label not in bboxes.keys():
					r = [i, j, 1, 1]
					bboxes[label] = r
				else:
					r = bboxes[label]
					x = r[0] + r[2] - 1
					y = r[1] + r[3] - 1
					if i < r[0]:
						r[0] = i 
					if i > x:
						x = i 
					if j < y:
						r[1] = j
					if j > y:
						y = j 
					r[2] = x - r[0] + 1
					r[3] = y - r[1] + 1
			i += 1
		j += 1
	return bboxes

def paint_rectangles(img, bboxes):
	for bbox in bboxes.values():
		start_point = (bbox[0], bbox[1])
		end_point = (bbox[0] + bbox[2], bbox[1] + bbox[3])
		img = cv2.rectangle(img, start_point, end_point, (0, 0, 255), 2)
			
	return img

def recall(TP, FN):
	if TP == 0 and FN == 0:
		return 0.0
	return TP / float(TP + FN)
	
def precision(TP, FP):
	if TP == 0 and FP == 0:
		return 0.0
	return TP / float(TP + FP)

def F1_score(precision, recall):
	return 2 * precision * recall / (precision + recall)

def rect_distance(r0, r1):
	dx = r0[0] + r0[2] / 2 - r1[0] - r1[2] / 2
	dy = r0[1] + r0[3] / 2 - r1[1] - r1[3] / 2
	return np.sqrt(dx ** 2 + dy ** 2)

def get_nearest_from_center(r, bboxes):
	if len(bboxes.keys()) == 0:
		return (0, 0, 0, 0)
	first = list(bboxes.keys())[0]

	nearest_index = first
	min_dist = rect_distance(r, bboxes[first])
	for key, bbox in bboxes.items():
		if key == first:
			continue
		dist = rect_distance(r, bbox)
		if dist < min_dist:
			min_dist = dist
			nearest_index = key
			
	return bboxes[nearest_index]

def IoU(r0, r1):
	maxx = r0[0] + r0[2]
	maxy = r0[1] + r0[3]

	im = np.zeros((maxy, maxx), np.uint8)
	
	start_point = (r0[0], r0[1])
	end_point = (r0[0] + r0[2], r0[1] + r0[3])    
	im = cv2.rectangle(im, start_point, end_point, (255), -1)

	start_point = (r1[0], r1[1])
	end_point = (r1[0] + r1[2], r1[1] + r1[3])
	im = cv2.rectangle(im, start_point, end_point, (255), -1)
	
	x = np.max((r0[0], r1[0]))
	y = np.max((r0[1], r1[1]))
	w = np.min((r0[0] + r0[2], r1[0] + r1[2])) - x
	h = np.min((r0[1] + r0[3], r1[1] + r1[3])) - y
	
	inter_area = np.abs(w * h)
	union_area = cv2.countNonZero(im)
	return inter_area / union_area

def evaluate_result(eval_mask, result):    
	if len(eval_mask.shape) == 3:
		mask = cv2.cvtColor(eval_mask, cv2.COLOR_BGR2GRAY)
	else:
		mask = eval_mask
			
	_, bin_mask = cv2.threshold(mask, 128, 255, cv2.THRESH_BINARY)
	cv2.imshow('Eval mask', bin_mask)
	
	cv2.imwrite('result.png', result)

	_, result = cv2.threshold(result, 128, 255, cv2.THRESH_BINARY)
	_, labels, _, _ = cv2.connectedComponentsWithStats(bin_mask)
	
	bboxes = get_blobs(labels)
	color_bmask = cv2.cvtColor(bin_mask, cv2.COLOR_GRAY2BGR)
	
	color_bmask = paint_rectangles(color_bmask, bboxes)
	cv2.imshow('Eval mask + Bounding Boxes', color_bmask)
	
	_, labels2, _, _ = cv2.connectedComponentsWithStats(result)
	
	bboxes2 = get_blobs(labels2)
	color_result = cv2.cvtColor(result, cv2.COLOR_GRAY2BGR)
	
	color_result = paint_rectangles(color_result, bboxes2)
	cv2.imshow('Result mask + Bounding Boxes', color_result)
	cv2.imwrite('result_bbox.png', color_result)
	
	TP, TN, FP, FN = 0, 0, 0, 0

	color_gstats = np.zeros((mask.shape[0], mask.shape[1], 3), np.uint8)
	for i in range(mask.shape[0]):
		for j in range(mask.shape[1]):
			GT = mask[i][j]
			ALG = result[i][j]
			if GT == 255 and ALG == 255:
				TP += 1
				color_gstats[i][j][1] = 255
			elif GT == 0 and ALG == 255:
				FP += 1
				color_gstats[i][j][2] = 255
			elif GT == 255 and ALG == 0:
				color_gstats[i][j][0] = 255
				color_gstats[i][j][1] = 255
				FN += 1
			else:
				TN += 1

	len_bbox = len(bboxes.keys())
	len_bbox2 = len(bboxes2.keys())

	print("GLOBAL STATS:")
	print("Color stats: GREEN for TP, RED for FP, YELLOW for FN, BLACK for TN.")
	print("\tTP: {}".format(TP))
	print("\tFP: {}".format(FP))
	print("\tFN: {}".format(FN))
	print("\tTN: {}".format(TN))

	prec = precision(TP, FP)
	rec = recall(TP, FN)
	fscore = F1_score(prec, rec)

	print("\tPrecision: {}".format(prec))
	print("\tRecall: {}".format(rec))
	print("\tF1-score: {}".format(fscore))
	print("\tApples in ground-thruth: {}".format(len_bbox))
	print("\tApples detected: {}".format(len_bbox2))
	print("\tDetection error: {:.2f}%".format((len_bbox - len_bbox2) * 100.0 / len_bbox))

	cv2.imshow('Color Stats', color_gstats)
	cv2.imwrite('color_stats.png', color_gstats)

	if len_bbox != 0 and len_bbox2 == 0:
		print('\tThere are {} bounding boxes in groundtruth, but you detected none. Considered as a mean IoU = 0.'.format(len_bbox))
	else:
		print('\tMean IoU calculated considering the nearest detected bounding box to each ground-truth bounding box.')
		IoUs = []
		mean_IoU, sd_IoU = 0.0, 0
		for key, bbox in bboxes.items():
			nearest = get_nearest_from_center(bbox, bboxes2)
			vIoU = IoU(nearest, bbox)
			mean_IoU += vIoU
			IoUs.append(vIoU)
			print('\tFor bounding box {} in groundtruth, IoU is: {}'.format(key, vIoU))
		mean_IoU /= len_bbox
		print('\tMean IoU: {}'.format(mean_IoU))

		for iou in IoUs:
			sd_IoU += (iou - mean_IoU) * (iou - mean_IoU)
		sd_IoU /= (len_bbox - 1)
		sd_IoU = np.sqrt(sd_IoU)
		print('\tStandard Deviation IoU: {}'.format(sd_IoU))

# Comienzo programa
# Se lee imagen de manzanas
apple = cv2.imread('apples.png')
apple_yuv = cv2.cvtColor(apple, cv2.COLOR_BGR2YUV)

# Se lee el canal V del espacio de color
apple_src = 255 - apple_yuv[:, :, 1]
apple_filt = cv2.medianBlur(apple_src, 9)
# Se segmenta con adaptive threshold
apple_seg = cv2.adaptiveThreshold(apple_filt, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 51, 0)

# Se utiliza el laplaciano de la segmentación pues entre pruebas, no fusionó los bordes entre manzanas.
laplacian = cv2.Laplacian(apple_seg, cv2.CV_64F)
ret, thresh = cv2.threshold(laplacian, 0, 255.0, cv2.THRESH_TOZERO)
borders = np.uint8(thresh)

# Se buscan los contornos externos de las figuras segmentadas
(contornos, hierarchy) = cv2.findContours(borders.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Se descartan aquellas figuras con un área muy pequeña descrita por el contorno
apple_cont = []
for c in contornos:
	if cv2.contourArea(c) > 3000.0:
		apple_cont.append(c)

# Se dibujan los contornos seleccionados con relleno
apple_cont = np.array(apple_cont)
dest = np.zeros(apple_seg.shape)
cv2.drawContours(dest, apple_cont, -1, (255), thickness=cv2.FILLED)

# Se compara el resultado con la máscara
mask = cv2.imread('apples_mask.png')
evaluate_result(mask, np.uint8(dest))
cv2.waitKey(0)