El certamen ha sido desarrollado en python. Instalar la biblioteca [scipy] y [opencv] para python. Si ya se encuentran instaladas estas dependencias en su computador ignorar este paso.
```sh
$ pip install numpy scipy opencv-python
```
#### Pregunta 2
Dentro del directorio correspondiente a esta pregunta se encuentran todos los archivos de salida ya generados, junto con un archivo stats.txt que posee la salida de la evaluación respecto al ground-truth. Para ejecutar el programa simplemente escribir en el terminal mientras se encuentra en el directorio Pregunta2:
```sh
$ python pregunta2.py
```
#### Pregunta 3
La versión de AULA no posee los samples de la secuencia puesto que el archivo comprimido de entrega excede el tamaño máximo de la plataforma. En el [repositorio] se encuentran todos los archivos necesarios para correr el algoritmo desde cero. Estando en el directorio Pregunta3:

- Para imprimir los resultados de evaluación ya evaluados ejecutar:
    ```sh
    $ python pregunta3.py
    ```
- Para visualizar las cajas englobantes del algoritmo y ground-truth
    ```sh
    $ python pregunta3.py -vis
    ```
- Para volver a correr el algoritmo, visualización y evaluación de resultados
    ```sh
    $ python pregunta3.py -all
    ```

[scipy]: <https://www.scipy.org/>
[anaconda]: <https://www.anaconda.com/>
[opencv]: <https://pypi.org/project/opencv-python/>
[repositorio]: <https://gitlab.com/pablo_rr/ipd441-cp1>